const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

function check(books) {
  try {
    printbooks();
    let notbooks = [];
    for (let i = 0; i < books.length; i++) {
      if (Object.keys(books[i]).length !== 3) {
        notbooks.push(books[i]);
      }
    }

    if (notbooks.length > 0) {
      let keyValues = ["author", "name", "price"];
      for (let i = 0; i < notbooks.length; i++) {
        for (let j = 0; j < keyValues.length; j++) {
          if (!Object.keys(notbooks[i]).includes(keyValues[j])) {
            notbooks[i][keyValues[j]] = "undefined";
          }
        }
      }

      throw new Error(
        `Неповні характеристики: ${JSON.stringify(notbooks, null, " ")}`
      );
    }
  } catch (error) {
    console.log(error);
  }
}

function printbooks() {
  const wrightBooks = books.filter(
    (element) => Object.keys(element).length === 3
  );

  const root = document.querySelector("#root");
  const ul = document.createElement("ul");
  root.appendChild(ul);

  for (let i = 0; i < wrightBooks.length; i++) {
    const li = document.createElement("li");
    li.innerHTML = `${wrightBooks[i].author} - ${wrightBooks[i].name} - ${wrightBooks[i].price}`;
    ul.appendChild(li);
  }
}

check(books);
